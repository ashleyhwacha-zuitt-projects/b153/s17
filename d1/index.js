/*
 Arrays are decalred using square brackets  [], also known as Array Literals

 Values inside of an array are called "elements"
*/

let grades = [98.5, 94.3, 89.2, 90.1];

// adding new value to grades array
 //grades[4] = 97.3
 //grades[grades.length] = 88.8

//console.log(grades)

//grades[3] = 80.1

//console.log(grades)

//console.log(grades.length)

/*let x =1;

console.log(x);

x = 100;

console.log(x);*/

let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo','Redfox', 'Gateway', 'tOSHIBA', 'Fujitsu'];

let tasks = ['shower', 'east breakfast', 'go to work', 'go home', 'go to sleep'];

let singleElement = [1];

let emptyArr = [];

let example1 = ["cat", "cat", "cat"];



/*
READING FROM/ACCESSING ELEMENTS INSIDE AN ARRAY

To access an array element, or simply denote the elements index number inside a pair of square brackets next to the name/variable of the array holding it

to get an elements index, simply count starting from zeroform the first element of an array

console.log(computerBrands[3]);

GETTING AN ELEMENTS INDEX

console.log(grades.indexOf(89.2))

console.log(grades.indexOf(70))

if indexOf cannot find your specified element inside of an array, it returns -1


if(emails.indexOf("jino@MAIL.COM") === -1){
	//Uuser can register , becasue jino@mail.com does not exist inside of the email array (indexOf returned -1)
}



ARRAY METHODS
(Method is just another word for function)
*/

//.push() - adds an element at end of an array AND returns the arrays length

//grades.push(88.8)

// console.log(grades)

//console.log(grades.push(88.8))


//.pop() - removes an arrays LAST element and returns the removed element

/*grades.pop()

console.log(grades)*/

//console.log(grades.pop())

grades.push(grades.pop())

console.log(grades)

let classA = ["john", "jacob", "jack"]
let classB = []


//.unshift() - Adds an element to the beginning and returns an arrays new length

// grades.unshift(77.7)

// console.log(grades)

//.shift - removes an arrays FIRST element AND returns the removed element

// grades.shift()

// console.log(grades)

//.splice() - Can add and remove from anywhere in an array , and even do both at once

// syntax: .splice(starting index number, number of items to remove, items to add <optional>)


// //ADD WITH SPLICE
// tasks.splice(3, 0, "eat lunch")

// console.log(tasks)

// //REMOVE WITH SPLICE
// tasks.splice(2, 1)

// //add and remove with splice

// tasks.splice(3, 2, "code", "code some more")

// console.log(tasks)

//.sort() - rearreanges elements in alphanumeric order

//computerBrands.sort();

//.reverse()
//computerBrands.reverse()
 //console.log(computerBrands)

 /*let arr1 = [1, 2, 3, 4]
 let arr2 = [5, 6, 7, 8, 9]

 let a = arr1.slice(0, 4)

 console.log(a)

 let b = arr2.splice(1, 3, 11)

console.log(b)*/

// let numbersArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

// let placeholderArr = []

// for(let i = 0; i < numbersArr.length; i++){
// 	if (numbersArr[i] % 2 === 0){
//       placeholderArr += numbersArr[i]
// 	}
// }

// console.log(placeholderArr)

//ARRAY ITERATIONMETHODS

//.forEach

// computerBrands.forEach(function(brand){
// 	console.log(brands)
// })

//.map() - creates a new array with the results of running a function on each element of the mapped array

let numbersArr = [1, 2, 3, 4, 5]

let doubledNumbers = numbersArr.map(function(number){
	return number * 2
})

console.log(doubledNumbers)